@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                <div class="panel-body">
                    <div class="centered">
                        @if(count($projects) > 0)
                        <div class="dropdown">
                          <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Select Project
                          <span class="caret"></span></button>
                         
                           
                              <ul class="dropdown-menu">
                               @foreach ($projects as $project)
                                <li>
                                    <a href="{{route('viewFromProject', ['project_id'=>$project->project_id])}}">
                                        {{ $project->project_name }}
                                    </a>
                                </li>
                               @endforeach
                              </ul>
                            @else
                                <p>no projects available at the moment. </p>
                           
                        </div>
                        @endif
                    </div>
                     
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
