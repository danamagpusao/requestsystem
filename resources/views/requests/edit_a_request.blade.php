@extends('layouts.app')

@section('content')
<div class="container">
    @include("errors.validation")
    
    @if(Auth::user()->user_type == "requestor" && $oldReq != null || Auth::user()->user_type == "approver" && $oldReq != null  || Auth::user()->user_type == "executor" && $oldReq != null)
    
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Request</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('modify_request') }}">
                       <input type="hidden" value="{{Session::token()}}" name="_token">
                           
                        <input type="hidden" value="{{ $oldReq->request_id }}" name="edit_id">
                        <input type="hidden" value="{{ $oldReq->area_group }}" name="area_group">
                        <input type="hidden" value="{{ $oldReq->series_no }}" name="series_no">
                        
                        <input type="hidden" value="{{ $oldReq->requestor_id }}" name="requestor_id">
                        <input type="hidden" value="{{ $oldReq->request_date }}" name="request_date">
                        <input type="hidden" value="{{ $oldReq->requestor_remarks }}" name="requestor_remarks">
                        
                        <input type="hidden" value="{{ $oldReq->project_id }}" name="project_id">
                        
                        <div class="form-group">
                            <label for="service_class" class="col-md-4 control-label">Service Class</label>
                            <div class="col-md-6">
                                <input id="service_class" value = "{{ $oldReq->service_class }}" type="text" class="form-control" name="service_class">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Size</label>
                            <div class="col-md-6">
                                <input id="size" value = "{{ $oldReq->size }}" value = "$oldReq->service_class" type="text" class="form-control" name="size">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="component" class="col-md-4 control-label">Component</label>
                            <div class="col-md-6">
                                <input id="component" value = "{{ $oldReq->component }}" type="text" class="form-control" name="component">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="description" class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                <input id="description" value = "{{ $oldReq->description }}" type="text" class="form-control" name="description">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="existence" class="col-md-4 control-label">Existence in Service Class</label>
                            <div class="col-md-6">
                                <input id="existence" value = "{{ $oldReq->existence_in_service_class }}" type="text" class="form-control" name="existence">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="dimension_table" class="col-md-4 control-label">Dimension Table</label>
                            <div class="col-md-6">
                                <input id="dimension_table" value = "{{ $oldReq->dimension_table }}" type="text" class="form-control" name="dimension_table">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="err_msg" class="col-md-4 control-label">Error Message</label>
                            <div class="col-md-6">
                                <input id="err_msg" value = "{{ $oldReq->error_message }}" type="text" class="form-control" name="err_msg">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="line_no" class="col-md-4 control-label">Line Number</label>
                            <div class="col-md-6">
                                <input id="line_no" value = "{{ $oldReq->line_no }}"  type="text" class="form-control" name="line_no">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="pid" class="col-md-4 control-label">P&ID Number</label>
                            <div class="col-md-6">
                                <input id="pid" value = "{{ $oldReq->pID_no }}" type="text" class="form-control" name="pid">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="area_code" class="col-md-4 control-label">Area Code</label>
                            <div class="col-md-6">
                                <input id="area_code" value = "{{ $oldReq->area_code }}" type="text" class="form-control" name="area_code">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="ident_code" class="col-md-4 control-label">Ident Code</label>
                            <div class="col-md-6">
                                <input id="ident_code" value = "{{ $oldReq->ident_code }}" type="text" class="form-control" name="ident_code">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="rating" class="col-md-4 control-label">Rating</label>
                            <div class="col-md-6">
                                <input id="rating" value = "{{ $oldReq->rating }}" type="text" class="form-control" name="rating">
                            </div>
                        </div>
                        @endif
                        
                                                
                        <!--- Requestor Only Fields --->
                        @if (Auth::user()->user_type == "requestor" && $oldReq->approver_id == null && $oldReq->executor_remarks == null)
                        
                        <div class="form-group">
                            <label for="area_group" class="col-md-4 control-label">Area Group</label>
                            <div class="col-md-6">
                                <input id="area_group" type="text" value="{{ $oldReq->area_group }}" class="form-control" name="area_group">
                            </div>
                        </div>
                        
                        
                        <!--- Approver Only Fields --->
                        @elseif (Auth::user()->user_type == "approver" && $oldReq->requestor_id != null && $oldReq->approver_id != null && $oldReq->executor_id == null)
                        <div class="form-group">
                            <label for="approval_state" class="col-md-4 control-label">Approve ?</label>
                            <div class="col-md-6">
                                <input id="approval_state" value="{{ $oldReq->approval_state }}" type="text" class="form-control" name="approval_state">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>
                            <div class="col-md-6">
                               <textarea class="form-control" rows="3" name="remarks">{{ $oldReq->approver_remarks }}</textarea>
                            </div>
                        </div>
                        
                        <!--- Executor Only Fields --->
                        @elseif (Auth::user()->user_type == "executor" && $oldReq->requestor_id != null && $oldReq->approver_id!=null && $oldReq->executor_id != null)
                        <input type="hidden" value="{{ $oldReq->approver_id }}" name="approver_id">
                        <input type="hidden" value="{{ $oldReq->approver_date }}" name="approver_date">
                        <input type="hidden" value="{{ $oldReq->approval_state }}" name="approval_state">
                        <input type="hidden" value="{{ $oldReq->approver_remarks }}" name="approval_remarks">
                        
                        <div class="form-group">
                            <label for="execution_state" class="col-md-4 control-label">Execute?</label>
                            <div class="col-md-6">
                                <input id="execution_state" value="{{ $oldReq->execution_state }}" type="text" class="form-control" name="execution_state">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>
                            <div class="col-md-6">
                               <textarea class="form-control" rows="3" name="remarks">{{ $oldReq->executor_remarks }}
                               </textarea>
                            </div>
                        </div>
                        
                        @endif
                         <div class="form-group">
                            <label for="attachments" class="col-md-4 control-label">Attachment/s</label>
                            <div class="col-md-6">
                                <input type="file" id="attachments"  name="attachments[]" multiple="multiple">
                            </div>
                        </div>
                        <button type = "submit" name="submit" class="btn btn-success"> 
                            <span class="glyphicon glyphicon-floppy-disk" aria-hidden="true" style="font-size: 12pt"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
