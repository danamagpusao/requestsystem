@extends('layouts.app')

@section('content')
<style>
    .content {
        padding:0px;
        width:90%;
        margin-left: auto;
        margin-right: auto;
    }
    article {
        float:left;
        width: calc(33.3333333% - 32px);
        margin: 12px;
        padding: 10px;
        border: solid gray 1px;
    }

    .edit_fld {
        opacity: 0.5;
        bottom: 30px;
        right: 30px;
        position: fixed;
        
    }
    .edit_fld:hover{
        opacity: 1.0;
    }
    
    .btn{
        margin: 5px;
    }
    
    
    
</style>
<section class="content">
@if ( count($customizationRequests) > 0 )
@foreach($customizationRequests as $customizationRequest)
    @if ($customizationRequest->approver_id != null && $customizationRequest->executor_id == null)
    <article class="request_status_div">
        <table class="table">
            <tr>
                <th>Requestor</th>
                <td>{{ $customizationRequest->user()->first()['fname'] }} {{ $customizationRequest->user()->first()['lname'] }}</td>
            </tr>
            <tr>
                <th>Request Date</th>
                <td>{{ $customizationRequest->request_date }}</td>
            </tr>
            <tr>
                <th>Approver</th>
                <td>{{App\User::find($customizationRequest->approver_id)->fname}} {{App\User::find($customizationRequest->approver_id)->lname}}</td>
            </tr>
            <tr>
                <th>Approval Date</th>
                <td>{{ $customizationRequest->approver_date }}</td>
            </tr>
            <tr>
                <th>Remarks</th>
                <td>{{ $customizationRequest->approver_remarks}}</td>
            </tr>
        </table>
    </article> 
    
    @endif
    <article class="requestor_details_div">
        @if ($customizationRequest->approver_id == null && $customizationRequest->executor_id == null)
           <span class="t_title"> Requestor Data </span> 
        @elseif ($customizationRequest->approver_id != null && $customizationRequest->executor_id == null)
            <span class="t_title"> Approver Data </span> 
        @elseif  ($customizationRequest->approver_id != null && $customizationRequest->executor_id != null) 
        <span class="t_title"> Executor Data </span>
        @endif
        
        <table class="table table-striped">
          <tr>
              <th>Series No.</th>
              <td>{{ $customizationRequest->series_no}}</td>
          </tr>
          <tr>
              <th>Service Class</th>
              <td>{{ $customizationRequest->service_class}}</td>
          </tr>
          <tr>
              <th>Size</th>
              <td>{{ $customizationRequest->size}}</td>
          </tr>
          <tr>
              <th>Component</th>
              <td>{{ $customizationRequest->component}}</td>
          </tr>
          <tr>
              <th>Description</th>
              <td>{{ $customizationRequest->description}}</td>
          </tr>
          <tr>
              <th>Existence in Service Class</th>
              <td>{{ $customizationRequest->existence_in_service_class}}</td>
          </tr>
          <tr>
              <th>Dimension Table</th>
              <td>{{ $customizationRequest->dimension_table}}</td>
          </tr>
          <tr>
              <th>Error Message</th>
              <td>{{ $customizationRequest->error_message}}</td>
          </tr>
          <tr>
              <th>Line No.</th>
              <td>{{ $customizationRequest->line_no}}</td>
          </tr>
          <tr>
              <th>P&ID No.</th>
              <td>{{ $customizationRequest->pID_no}}</td>
          </tr>
          <tr>
              <th>Area Code</th>
              <td>{{ $customizationRequest->area_code}}</td>
          </tr>
          <tr>
              <th>Ident Code</th>
              <td>{{ $customizationRequest->ident_code}}</td>
          </tr>
          <tr>
              <th>Rating</th>
              <td>{{ $customizationRequest->rating}}</td>
          </tr>
          <tr>
              <th>File Attachments</th>
              <td>
                  <ul>
                @foreach( App\Fileentry::where('request_id',$customizationRequest->request_id)->get() as $file )
                   <li> <a target="_blank" href="/download/{{$file->filename}}"> {{$file->original_filename }} </a> </li>
                @endforeach
                  </ul>
              </td>
          </tr>
        </table>
    </article>
    

    @if  ($customizationRequest->approver_id != null && $customizationRequest->executor_id != null) 
    <article class="completion_status_div">
        <table class="table">
           <tr>
                <th>Executor</th>
                <td>{{App\User::find($customizationRequest->executor_id)->fname}} {{App\User::find($customizationRequest->executor_id)->lname}}</td>
            </tr>
            <tr>
                <th>Completion Status</th>
                <td>{{ $customizationRequest->status }}</td>
            </tr>
            <tr>
                <th>Date & Time</th>
                <td>{{ $customizationRequest->execution_date }}</td>
            </tr>
            <tr>
                <th>Remarks</th>
                <td>{{ $customizationRequest->executor_remarks }}</td>
            </tr>
        </table>
    </article>
    @endif
    @endforeach

    @endif
    </section>
    
    <section class="edit_fld" >
        @if (Auth::user()->user_type == "requestor" && $customizationRequest->approver_id == null && Auth::user()->id == $customizationRequest->requestor_id
        || Auth::user()->user_type == "approver" && $customizationRequest->approver_id != null && $customizationRequest->executor_id == null && Auth::user()->id == $customizationRequest->approver_id
        || Auth::user()->user_type == "executor" && $customizationRequest->executor_id != null && $customizationRequest->approver_id != null && Auth::user()->id == $customizationRequest->executor_id && $customizationRequest->status != "completed")
               <a href="{{route('edit_request', ['project_id' => $project_id, 'oldReq' => $customizationRequest->request_id])}}"><button class="btn btn-warning"> Edit Data </button>
        @endif
        
        @if (Auth::user()->user_type == "approver" && $customizationRequest->approver_id == null && $customizationRequest->executor_id == null)
              <a href="{{route('add_request', ['project_id' => $project_id, 'oldReq' => $customizationRequest->request_id])}}"> <button class="btn btn-info"> Approve </button> </a>  
        @endif
        
        @if (Auth::user()->user_type == "executor" && $customizationRequest->approver_id != null )
              @if($customizationRequest->executor_id == null)
              <a href="{{route('add_request_executor', ['project_id' => $project_id, 'oldReq' => $customizationRequest->request_id])}}"> <button class="btn btn-info"> Execute </button></a>
              @elseif($customizationRequest->status == "ongoing")
              <form class="form-horizontal" role="form" method="POST" action="{{route('complete_request', ['request_id' => $customizationRequest->request_id])}}">
                  {{ csrf_field() }} 
                  <button type ="submit" class="btn btn-info"> Complete </button>
              </form>
            @endif
        @endif
        <button class="btn" onclick="goBack()">Back</button>
     
    </section> 
       <script>
            function goBack() {
                window.history.back()
            }
         </script>  
@endsection