@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row">
        
        <div>
            <ul class="nav nav-tabs nav-justified">
                      @if($active == null)
              <li role="presentation" class="active">
              @else
              <li role="presentation">
              @endif
                  <a href="{{ route('viewFromProject', ['project_id'=>$project_id]) }}">All</a>
              </li>

              
              @if($active == 'completed')
              <li role="presentation" class="active">
              @else
              <li role="presentation">
              @endif
                  <a href="{{ route('viewFromProject', ['project_id'=>$project_id,'filter'=>'completed']) }}">Completed</a>
              </li>
      
      
              @if(Auth::user()->user_type == "approver" )
                  @if($active == 'disapproved')
                  <li role="presentation" class="active">
                  @else
                  <li role="presentation">
                  @endif
                      <a href="{{ route('viewFromProject', ['project_id'=>$project_id,'filter'=>'disapproved']) }}">Disapproved</a>
                  </li>
                  @if($active == 'rejected')
                  <li role="presentation" class="active">
                  @else
                  <li role="presentation">
                  @endif
                      <a href="{{ route('viewFromProject', ['project_id'=>$project_id,'filter'=>'rejected']) }}">Rejected</a>
                  </li>
              @endif

              
              
              @if(Auth::user()->user_type == "executor")
                  @if($active == 'for execution')
                  <li role="presentation" class="active">
                  @else
                  <li role="presentation">
                  @endif
                      <a href="{{ route('viewFromProject', ['project_id'=>$project_id,'filter'=>'for execution']) }}">For Execution</a>
                  </li>
                  
                  @if($active == 'ongoing')
                  <li role="presentation" class="active">
                  @else
                  <li role="presentation">
                  @endif
                      <a href="{{ route('viewFromProject', ['project_id'=>$project_id,'filter'=>'ongoing']) }}">Ongoing</a>
                  </li>
              @endif
            </ul>
            
            @if (count ($customizationRequests) > 0 )
            <table class="table table-hover">
              <tr>
                  <th>
                      Reference No.
                  </th>
                  <th>
                      Requestor
                  </th>
                  <th>
                      Service Class
                  </th>
                  <th>
                      Component
                  </th>
                  <th>
                      Size 
                  </th>
                  <th>
                      Area
                  </th>
                  <th>
                      Status
                  </th>
              </tr>
              @foreach($customizationRequests as $customizationRequest)
                  <tr>
                      <td>
                         <a href="{{route('viewOne', ['project_id' => $project_id,'customizationRequest'=>$customizationRequest->request_id  ])}}">
                             {{$customizationRequest->series_no}}
                         </a> 
                      </td>
                      <td>
                          {{ App\User::find($customizationRequest->requestor_id)->fname }} {{App\User::find($customizationRequest->requestor_id)->lname}}
                      </td>
                      <td>
                          {{$customizationRequest->service_class}}
                      </td>
                      <td>
                          {{$customizationRequest->component}}
                      </td>
                      <td>
                          {{$customizationRequest->size}}
                      </td>
                      <td>
                          {{$customizationRequest->area_code}}
                      </td>
                      <td>
                          {{$customizationRequest->status}}
                      </td>
                  </tr>
              @endforeach
            </table>
            @else
                <p class="empty">No request for this project yet.</p>
            @endif
            <div class="add_request_btn" style="display: inline; position:relative;">
                <a  href="{{route('add_request', ['project_id' => $project_id])}}"><button class="btn btn-primary">New Request</button></a>
            </div>
        </div>
    </div>
{!! $customizationRequests->render() !!}
    
</div>
@endsection
