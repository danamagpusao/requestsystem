@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="content" style="margin-top:10%">
        <!---- icons ---->
        <a href="{{ route('view_users') }}">
            <div class ="admin_icon btn-warning" id="users_icon">
                <span class="glyphicon glyphicon-user" aria-hidden="true" style="font-size:90px;"></span><br>
                Users
            </div>
        </a>
        <a href="{{ route('view_all_projects') }}">
        <div class ="admin_icon btn-primary" id="projects_icon">
            <span class="glyphicon glyphicon-briefcase" aria-hidden="true" style="font-size:90px;"></span><br>
            Projects
        </div>
        </a>
        <a href="{{ route('view_groups') }}">
        <div class ="admin_icon btn-danger" id="groups_icon">
            <span class="glyphicon glyphicon-th-large" aria-hidden="true" style="font-size:90px; position:relative; "></span><br>
            Groups
        </div>
        </a>
        
        <a href="{{ route('email_logs') }}">
        <div class ="admin_icon btn-success" id="groups_icon">
            <span class="glyphicon glyphicon-envelope" aria-hidden="true" style="font-size:90px; position:relative; "></span><br>
            Email Logs
        </div>
        </a>
    </div>
</div>
@endsection
