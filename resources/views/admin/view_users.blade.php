@extends('layouts.admin')
<style> 
  

</style>
@section('content')
<div class="container">
    <div class="active" style="margin: 0px;">
        <div class="panel panel-default">
          <!-- Default panel contents -->
          <div class="panel-heading">Active Users</div>
          <div class="panel-body" style="max-height: 500px ; overflow-y:auto;">


        
          <!-- Table -->
          @if(count($activeUsers) > 0)
          <table class="table">
            <tr>
              <th>Name</th>
              <th>User Type</th>
              <th>Email</th>
              <td></td>
              <td></td>
            </tr>
            
              @foreach ($activeUsers as $activeUser)
                  <tr>
                    <td>{{ $activeUser->lname }}, {{ $activeUser->fname }}</td>
                    <td>{{ $activeUser->user_type }}</td>
                    <td>{{ $activeUser->email }}</td>
                    <td>
                      <a href="{{ route('edit_user', ['user_id'=>$activeUser->id]) }}">
                        <button class="btn btn-warning">
                          <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                        </button>
                      </a>
                    </td>
                    <td>
                      <a href="{{ route('deactivate', ['user_id'=>$activeUser->id]) }}">
                        <button class="btn btn-danger">
                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                        </button>
                      </a>
                    </td>
                  </tr>
              @endforeach
          </table>
          @else
                <p>No active users at the moment.</p>
            @endif
            </div>
        </div>
    </div>
    <div class="inactive">
        <div class="panel panel-default" style="margin: 0px;">
          <!-- Default panel contents -->
          <div class="panel-heading">Inactive Users</div>
          <div class="panel-body" style="max-height: 500px ; overflow-y:auto;">
            <!-- Table -->
            @if(count($inactiveUsers) > 0)
            <table class="table">
              <tr>
                <th>Name</th>
                <th>User Type</th>
                <th>Email</th>
                <td></td>
                <td></td>
              </tr>
              
                @foreach ($inactiveUsers as $inactiveUser)
                    <tr>
                      <td>{{ $inactiveUser->lname }}, {{ $inactiveUser->fname }}</td>
                      <td>{{ $inactiveUser->user_type }}</td>
                      <td>{{ $inactiveUser->email }}</td>
                      <td>
                        <a href="{{ route('edit_user', ['user_id'=>$inactiveUser->id]) }}">
                          <button class="btn btn-warning">
                            <span class="glyphicon glyphicon-edit" aria-hidden="true"></span>
                          </button>
                        </a>
                    </td>
                    <td>
                      <a href="{{ route('activate', ['user_id'=>$inactiveUser->id]) }}">
                          <button class="btn btn-success">
                            <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                          </button>
                      </a>
                    </td>
                    </tr>
                @endforeach
              
            </table>
            @else
                <p>No inactive users at the moment.</p>
              @endif
          </div>
        </div>
    </div>
    <a href="/admin">
      <button class="floating" style="
      bottom: 3%;
      left: 3%;
      ">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </button>
    </a>  
    <a href="{{route('add_user')}}"><button class="floating" style="
      bottom: 3%;
      right: 3%;
      "><span class="glyphicon glyphicon-plus" aria-hidden="true" style="font-size:15pt"></span> </button></a>
  
</div>
@endsection
