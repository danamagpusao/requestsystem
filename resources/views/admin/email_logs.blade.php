@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="active">
        <div class="panel panel-default">
          <!-- group name | #members -->
          <div class="panel-heading">Email Logs</div>
            <div class="panel-body">
                  <table class="table table-hover">
                    <tr>
                      <th>Recepient (Email)</th>
                      <th>Status</th>
                      <th>Date</th>
                    </tr>
                    @foreach($logs as $log)
                    <tr>
                        <td>{{ $log->recepient_email  }}</td>
                        <td>{{ $log->status }}</td>
                        <td>{{ $log->created_at }}</td>
                    </tr>
                    @endforeach
                  </table>
            </div>
          </div>
      </div>
      <a href="/admin">
      <button class="floating" style="
      bottom: 3%;
      left: 3%;
      ">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </button>
    </a>  
  
</div>
@endsection
