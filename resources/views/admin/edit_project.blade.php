@extends('layouts.admin')

@section('content')
<div class="container">
     <div class="panel panel-default">
        <div class="panel-heading">Edit Project</div>
        <div class="panel-body">
            <form class="form-horizontal" role="form" method="POST" action="{{ route('save_project',['project_id'=>$project->project_id]) }} ">
                {{ csrf_field() }} 
                <div class="form-group">
                    <label for="pname" class="col-md-4 control-label">Project Name</label>
                    <div class="col-md-6">
                        <input id="pname" value = "{{ $project->project_name }}" type="text" class="form-control" name="pname">
                            <button class="btn btn-success" type="submit">
                                save
                            </button>
                    </div>
                </div>
            </form>
            <div>
                <table class="table">
                <tr>
                    <th>Name</th>
                    <td>User Type</td>
                </tr>
                @foreach ($project->users()->orderBy('lname','asc')->get() as $member)
                <tr>
                    <td>{{ $member->lname }}, {{ $member->fname }}</td>
                    <td>{{ $member->user_type }}</td>
                    <td>
                        <a href="{{ route('remove_pmember', ['project_id'=> $project->project_id, 'member_id'=> $member->id]) }}">
                            <button class="btn btn-danger">remove</button>
                        </a>
                    </td>
                <tr>
                @endforeach
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
