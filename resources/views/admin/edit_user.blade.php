@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="row">
        @if(Auth::user()->user_type == 'admin' || Auth::user()->id == $currentUser->id )
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit User</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('update_user', ['user_id'=>$currentUser->id]) }}">
                        {{ csrf_field() }} 

                        <div class="form-group{{ $errors->has('fname') ? ' has-error' : '' }}">
                            <label for="fname" class="col-md-4 control-label">First Name</label>

                            <div class="col-md-6">
                                <input id="fname" type="text" class="form-control" name="fname" value="{{ $currentUser->fname }}">

                                @if ($errors->has('fname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('fname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('lname') ? ' has-error' : '' }}">
                            <label for="lname" class="col-md-4 control-label">Last Name</label>

                            <div class="col-md-6">
                                <input id="lname" type="text" class="form-control" name="lname" value="{{$currentUser->lname }}">

                                @if ($errors->has('lname'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('lname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ $currentUser->email }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <div class="form-group{{ $errors->has('user_type') ? ' has-error' : '' }}">
                            <label for="user_type" class="col-md-4 control-label">User Type</label>

                            <div class="col-md-6">
                                <select class="form-control" id="user_type" name="user_type">
                                @if($currentUser->user_type == "admin")
                                    <option value="admin" selected="selected">
                                @else  
                                    <option value="admin">
                                @endif
                                        Admin     
                                    </option>
                                @if($currentUser->user_type == "requestor")
                                    <option value="requestor" selected="selected">
                                @else  
                                    <option value="requestor">
                                @endif
                                        Requestor 
                                    </option>
                                    
                                @if($currentUser->user_type == "approver")
                                    <option value="approver" selected="selected">
                                @else  
                                    <option value="approver">
                                @endif
                                    Approver
                                    </option>
                                    
                                @if($currentUser->user_type == "executor")
                                    <option value="executor" selected="selected">
                                @else  
                                    <option value="executor">
                                @endif
                                    Executor
                                    </option>
                               </select>
                                @if ($errors->has('user_type'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('user_type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        
                        <br><br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                              <label for="sel2">Select Projects (hold shift to select more than one):</label>
                              <select  multiple class="form-control" id="sel2" name="projects[]">
                                 @foreach ($projects as $project)
                                    @if($currentUser->projects()->contains($project))
                                    <option value="{{ $project->project_id }}" selected="selected"> {{ $project->project_name }}</option>
                                    @else
                                    <option value="{{ $project->project_id }}"> {{ $project->project_name }}</option>
                                    @endif
                                @endforeach
                              </select>
                             </div>
                        </div>
                        <br>
                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                              <label for="sel2">Select Groups (hold shift to select more than one):</label>
                              <select  multiple class="form-control" id="sel2" name="groups[]">
                                 @foreach ($groups as $group)
                                    @if($currentUser->groups()->contains($group))
                                    <option value="{{ $group->group_id }}" selected="selected"> {{ $group->group_name }}</option>
                                    @else
                                    <option value="{{ $group->group_id }}"> {{ $group->group_name}}</option>
                                    @endif
                                @endforeach
                              </select>
                             </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-user"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        @else
        <p>Action not allowed</p>
        @endif
    </div>
</div>
@endsection
