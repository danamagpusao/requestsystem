@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="active">
        <div class="panel panel-default">
          <!-- group name | #members -->
          <div class="panel-heading">Groups</div>
            <div class="panel-body">
                  <table class="table table-hover">
                    <tr>
                      <th>Group Name</th>
                      <th>Members</th>
                    </tr>
                    @foreach($groups as $group)
                    <tr>
                        <td>{{ $group->group_name  }}</td>
                        <td>{{ $group->users()->get()->count() }}</td>
                        <td>
                          <a href="{{ route('edit_group',['group_id'=>$group->group_id]) }}">
                            <button class="btn btn-warning">
                                edit
                            </button>
                          </a>
                        </td>
                    </tr>
                    @endforeach
                  </table>
            </div>
          </div>
      </div>
      <a href="/admin">
      <button class="floating" style="
      bottom: 3%;
      left: 3%;
      ">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </button>
    </a>  
   <button data-toggle="modal" data-target=".add-group-modal" class="floating" style="
      bottom: 3%;
      right: 3%;
      "><span class="glyphicon glyphicon-plus" aria-hidden="true" style="font-size:15pt"></span> </button>
</div>

<div class="modal fade add-group-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create new Group</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form" method="POST" action="{{route('add_group')}}">
                {{ csrf_field() }} 
                <div class="form-group">
                    <label for="pname" class="col-md-4 control-label">Group Name</label>
                    <div class="col-md-6">
                        <input id="gname" type="text" class="form-control" name="gname">
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Add</button>
            </form>
        </div>
        </div><!-- /.modal-content -->
  </div>
@endsection
