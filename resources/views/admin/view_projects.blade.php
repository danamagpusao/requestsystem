@extends('layouts.admin')

@section('content')
<div class="container">
    <div class="active">
        <div class="panel panel-default">
          <!-- Default panel contents -->
          <!-- Project Name | #All Request | #For Approval | #Disapproved | #For Execution | #Ongoing | #Rejected -->
          <div class="panel-heading">Projects</div>
          <div class="panel-body">
            <table class="table table-hover">
                <tr>
                    <th>Project Name</th>
                    <th>All Requests</th>
                    <th>For Approval</th>
                    <th>Disapproved</th>
                    <th>For Execution</th>
                    <th>Ongoing</th>
                    <th>Rejected</th>
                </tr>
                @foreach( $projects as $project)
                <tr>
                    <td>{{ $project->project_name }}</td>
                    <td>{{ $project->customizationRequests()->get()->count() }}</td>
                    <td>{{ $project->customizationRequests()->where('status','for approval')->get()->count() }}</td>
                    <td>{{ $project->customizationRequests()->where('status','disapproved')->get()->count() }}</td>
                    <td>{{ $project->customizationRequests()->where('status','for execution')->get()->count() }} </td>
                    <td>{{ $project->customizationRequests()->where('status','ongoing')->get()->count() }}</td>
                    <td>{{ $project->customizationRequests()->where('status','rejected')->get()->count() }}</td>
                    <td>
                        <a href="{{ route('edit_project', ['project_id'=>$project->project_id]) }}">
                            <button class="btn btn-warning">
                                edit
                            </button>
                        </a>
                        
                        
                    </td>
                </tr>
                @endforeach
            </table>
          </div>
        </div>
    </div>
     <a href="/admin">
      <button class="floating" style="
      bottom: 3%;
      left: 3%;
      ">
        <span class="glyphicon glyphicon-arrow-left" aria-hidden="true"></span>
      </button>
    </a>  
    <button data-toggle="modal" data-target=".add-project-modal" class="floating" style="
      bottom: 3%;
      right: 3%;
      "><span class="glyphicon glyphicon-plus" aria-hidden="true" style="font-size:15pt"></span> </button>
</div>


<div class="modal fade add-project-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
  <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Create new Project</h4>
        </div>
        <div class="modal-body">
            <form class="form-horizontal" role="form" method="POST" action="{{route('add_project')}}">
                {{ csrf_field() }} 
                <div class="form-group">
                    <label for="pname" class="col-md-4 control-label">Project Name</label>
                    <div class="col-md-6">
                        <input id="pname" type="text" class="form-control" name="pname">
                    </div>
                </div>
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success">Add</button>
            </form>
        </div>
        </div><!-- /.modal-content -->
  </div>
</div>
@endsection
