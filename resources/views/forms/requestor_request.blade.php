<div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Request</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ route('store_request') }}">
                       <input type="hidden" value="{{Session::token()}}" name="_token">
                       <input type="hidden" value="{{$project_id}}" name="project_id">
                        <div class="form-group">
                            <label for="service_class" class="col-md-4 control-label">Service Class</label>
                            <div class="col-md-6">
                                <input id="service_class" type="text" class="form-control" name="service_class">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="size" class="col-md-4 control-label">Size</label>
                            <div class="col-md-6">
                                <input id="size" type="text" class="form-control" name="size">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="component" class="col-md-4 control-label">Component</label>
                            <div class="col-md-6">
                                <input id="component" type="text" class="form-control" name="component">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="description" class="col-md-4 control-label">Description</label>
                            <div class="col-md-6">
                                <input id="description" type="text" class="form-control" name="description">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="existence" class="col-md-4 control-label">Existence in Service Class</label>
                            <div class="col-md-6">
                                <input id="existence" type="text" class="form-control" name="existence">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="dimension_table" class="col-md-4 control-label">Dimension Table</label>
                            <div class="col-md-6">
                                <input id="dimension_table" type="text" class="form-control" name="dimension_table">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="err_msg" class="col-md-4 control-label">Error Message</label>
                            <div class="col-md-6">
                                <input id="err_msg" type="text" class="form-control" name="err_msg">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="line_no" class="col-md-4 control-label">Line Number</label>
                            <div class="col-md-6">
                                <input id="line_no" type="text" class="form-control" name="line_no">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="pid" class="col-md-4 control-label">P&ID Number</label>
                            <div class="col-md-6">
                                <input id="pid" type="text" class="form-control" name="pid">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="area_code" class="col-md-4 control-label">Area Code</label>
                            <div class="col-md-6">
                                <input id="area_code" type="text" class="form-control" name="area_code">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="ident_code" class="col-md-4 control-label">Ident Code</label>
                            <div class="col-md-6">
                                <input id="ident_code" type="text" class="form-control" name="ident_code">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="rating" class="col-md-4 control-label">Rating</label>
                            <div class="col-md-6">
                                <input id="rating" type="text" class="form-control" name="rating">
                            </div>
                        </div>
                        
                        <!--- Requestor Only Fields --->
                        @if (Auth::user()->user_type == "requestor")
                        
                        <div class="form-group">
                            <label for="area_group" class="col-md-4 control-label">Area Group</label>
                            <div class="col-md-6">
                                <input id="area_group" type="text" class="form-control" name="area_group">
                            </div>
                        </div>
                        
                        <!--- Approver Only Fields --->
                        @elseif (Auth::user()->user_type == "approver")
                        <div class="form-group">
                            <label for="approval_state" class="col-md-4 control-label">Approve ?</label>
                            <div class="col-md-6">
                                <input id="approval_state" type="text" class="form-control" name="approval_state">
                            </div>
                        </div>
                        
                        <!--- Executor Only Fields --->
                        @elseif (Auth::user()->user_type == "executor")
                        <div class="form-group">
                            <label for="execution_state" class="col-md-4 control-label">Execute?</label>
                            <div class="col-md-6">
                                <input id="remarks" type="text" class="form-control" name="completion_status">
                            </div>
                        </div>
                        
                        @endif
                       
                       <div class="form-group">
                            <label for="remarks" class="col-md-4 control-label">Remarks</label>
                            <div class="col-md-6">
                               <textarea class="form-control" rows="3" name="remarks"></textarea>
                            </div>
                        </div>                
                        
                        <button type = "submit" name="submit"> 
                            <span class="glyphicon glyphicon-heart" aria-hidden="true" style="font-size: 20pt"></span>
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>