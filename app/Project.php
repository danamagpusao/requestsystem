<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public $primaryKey = 'project_id';
    
    public function users(){
        return $this->belongsToMany(User::class,'project_user', 'project_id', 'user_id');
    }
    
    public function customizationRequests(){
        return $this->hasMany(CustomizationRequest::class);
    }
}
