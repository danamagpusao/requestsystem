<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Fileentry extends Model
{
    protected $table = 'fileentries';
    
    public function user() {
        return $this->belongsTo(User::class);
    }
    
    public function customizationRequest() {
        return $this->belongsTo(CustomizationRequest::class,'request_id');
    }
}
