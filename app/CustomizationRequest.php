<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;

use App\Http\Controllers\Controller;
use App\Fileentry;
use Request;
 


class CustomizationRequest extends Model
{
    protected $primaryKey = 'request_id';
    
    public function project(){
        return $this->belongsTo(Project::class);
    }
    
    public function file_attachments(){
        return $this->hasMany(FileAttachment::class);
    }
    
    public function user(){
        return $this->belongsTo(User::class, 'requestor_id');
    }
    
    public function fileentries(){
        return $this->hasMany(Fileentry::class,'request_id','id');
    }
}
