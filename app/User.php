<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'fname', 'lname', 'email','user_type', 'password',
    ];
    
    public function groups(){
        return $this->belongsToMany(Group::class,'user_group', 'user_id', 'group_id')->get();
    }
    
    public function customizationRequests(){
        return $this->hasMany(CustomizationRequest::class,'request_id');
    }
    
    public function fileentries(){
        return $this->hasMany(Fileentry::class);
    }
    
    public function projects(){
        return $this->belongsToMany(Project::class, 'project_user', 'user_id', 'project_id')->get();
    }

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    public function getType(){
        return $this->user_type;
    }
}
