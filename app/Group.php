<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    public $primaryKey = 'group_id';
    protected $table = 'groups';
    
    public function users(){
        return $this->belongsToMany(User::class,'user_group', 'group_id', 'user_id');
    }
}
