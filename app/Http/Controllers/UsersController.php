<?php

namespace App\Http\Controllers;
use App\Project;
use App\User;
use App\Group;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;


class UsersController extends Controller
{
    public function view(){
        return view('admin.view_users', [
            'activeUsers' => $this->getActive(),
            'inactiveUsers' => $this->getInactive(),
        ]);
    }
    public function viewAll(){ //returns all users in the db
        $users = User::all();
        return view('admin.view_users', [
            'users' => $users,
        ]);
    }
    
    public function getActive() {
        $activeUsers = User::where('isActive', 1)->orderBy('lname')->get();
        return $activeUsers;
    }
    
    public function getInactive() {
        $inactiveUsers = User::where('isActive', 0)->orderBy('lname')->get();
        return $inactiveUsers;
    }
    
    public function activate($user_id){ //activates user
       $user =  User::find($user_id);
       $user->isActive = 1;
       $user->save();
       return redirect('/admin/view_users');
    }
    
    public function deactivate($user_id){ //deactivates user account
     
       $user =  User::find($user_id);

       //checks if the user to be deactivated is an admin, if the # of admin is less than or equal to one, user cannot be deactivated
       if($user->user_type == "admin" && User::where('user_type','admin')->count()<=1) {
            return redirect('/admin/view_users');       
       }
       
       $user->isActive = 0;
       $user->save();
       return redirect('/admin/view_users');
    }
    
    public function saveUser(Request $request){
        
        $newUser = new User();
        
        $newUser->fname = $request->fname;
        $newUser->lname = $request->lname;
        $newUser->email = $request->email;
        $newUser->user_type =  $request->user_type;
        $newUser->password =  bcrypt($request->lname); 
        $newUser->save();
        
        if(count($request->projects) > 0 ) {
            foreach ($request->projects as $project_id){
               DB::table('project_user')->insert( array(
                'project_id' => $project_id,
                'user_id' => $newUser->id,
                )
            );
            }
        }
        
        if(count($request->groups) > 0 ) {
            foreach ($request->groups as $group_id){
                DB::table('user_group')->insert( array(
                    'group_id' => $group_id,
                    'user_id' => $newUser->id,
                    )
                );
            }
        }
        return redirect('/admin/view_users');
    }
    
    public function editUser($user_id){
        $user = User::find($user_id);
      
        return view('admin.edit_user', [
            'currentUser' => $user,
            'projects' => Project::all(),
            'groups' => Group::all(),
        ]);
    }
    
    public function updateUser(Request $request, $user_id) {
        $user = User::find($user_id);
        $user->fname = $request->fname;
        $user->lname = $request->lname;
        $user->email = $request->email;
        $user->save();

        if(count($request->projects) > 0 ) {
            foreach ($request->projects as $project_id){
               DB::table('project_user')->insert( array(
                'project_id' => $project_id,
                'user_id' => $user->id,
                )
            );
            }
        }
        
        if(count($request->groups) > 0 ) {
            foreach ($request->groups as $group_id){
                DB::table('user_group')->insert( array(
                    'group_id' => $group_id,
                    'user_id' => $user->id,
                    )
                );
            }
        }
        return redirect('/admin/view_users');
    }
    
    
    
}
