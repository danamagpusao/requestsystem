<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Http\Requests;
use Input;
use App\CustomizationRequest;
use App\User;
use App\Group;
use App\Fileentry;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
 

class CustomizationRequestController extends Controller
{
    public function storeRequest(Request $request) {
            
            $this->validate($request,[
                   'service_class' => 'required',
                   'size' => 'required',
                   'component' => 'required',
                   'existence' => 'required',
                   'dimension_table' => 'required',
                   'err_msg' => 'required',
                   'line_no' => 'required',
                   'pid' => 'required',
                   'area_code' => 'required',
                   'ident_code' => 'required',
            ]);
            $user_type = $request->user()->user_type;
            $customizationRequest = new CustomizationRequest();
            
            $customizationRequest->service_class = $request->service_class;
            $customizationRequest->size = $request->size;
            $customizationRequest->component = $request->component;
            $customizationRequest->description = $request->description;
            $customizationRequest->existence_in_service_class = $request->existence;
            $customizationRequest->dimension_table = $request->dimension_table;
            $customizationRequest->error_message = $request->err_msg;
            $customizationRequest->line_no = $request->line_no;
            $customizationRequest->pID_no = $request->pid;
            $customizationRequest->area_code = $request->area_code;
            $customizationRequest->ident_code = $request->ident_code;
            $customizationRequest->rating = $request->rating;
            
            $customizationRequest->project_id = $request->project_id;
            $customizationRequest->area_group = $request->area_group;
            
            if ($user_type == "requestor"){
                $customizationRequest->series_no = CustomizationRequest::where('project_id',$request->project_id)->distinct('series_no')->count()+1;
                
            }
            else $customizationRequest->series_no = $request->series_no;
            
            $user_type = $request->user()->user_type;
            if ($user_type == "requestor"){
                $customizationRequest->requestor_id = $request->user()->id;
                $customizationRequest->request_date = Carbon::now();
        //        $customizationRequest->requestor_remarks = $request->remarks;
                $customizationRequest->status = "for approval";
            }
            else {
                $customizationRequest->requestor_id = $request->requestor_id;
                $customizationRequest->request_date = $request->request_date;
                $customizationRequest->requestor_remarks = $request->requestor_remarks;
            
                if ($user_type == "approver") {
                    $customizationRequest->approver_id = $request->user()->id;
                    $customizationRequest->approver_date = Carbon::now();
                    $customizationRequest->approval_state = $request->approval_state;
                    $customizationRequest->approver_remarks = $request->remarks;
                    if($request->approval_state == "yes") {
                        $customizationRequest->status = "for execution";    
                    }
                    else{
                        $customizationRequest->status = "disapproved";
                    }
                }
            
                else if ($user_type == "executor") {
                    $customizationRequest->approver_id = $request->approver_id;
                    $customizationRequest->approver_date = $request->approver_date;
                    $customizationRequest->approval_state = $request->approval_state;
                    $customizationRequest->approver_remarks = $request->approver_remarks;
                    
                    $customizationRequest->executor_id = $request->user()->id;
                    $customizationRequest->execution_state = $request->execution_state;
                    $customizationRequest->execution_date = Carbon::now();
                    $customizationRequest->executor_remarks = $request->remarks;
                    
                    if($request->execution_state == "yes"){
                        $customizationRequest->status = "ongoing";
                    }
                    else{
                        $customizationRequest->status = "disapproved";
                    }
                    
                }
            }
            
            $customizationRequest->save();
            $this->addFile($request, $customizationRequest->request_id);
            $this->sendMail($request,$customizationRequest);
            return redirect ('/requests/'.$request->project_id.'/filter');
        
  }

    public function viewFromProject(Request $request, $project_id, $filter = null)
    {
        $req = array();
        $series_nos = CustomizationRequest::where('project_id',$project_id)->groupBy('series_no')->pluck('series_no');
        foreach ($series_nos as $series_no) {
            $chosen = CustomizationRequest::where('project_id',$project_id)->where('series_no', $series_no)->max('request_id');
            array_push($req, $chosen);
        }
        
        if($filter != null){
             $crs = DB::table('customization_requests')
                    ->whereIn('request_id', $req)->where('status',$filter)->paginate(3);    
        }
        else {
            $crs = DB::table('customization_requests')
                    ->whereIn('request_id', $req)->paginate(10);
        }
        return view('requests.view_requests', [
             'customizationRequests' => $crs,
             'project_id' => $project_id,
             'active' => $filter,
        ]);
    }
    
    
    public function viewOneRequest(Request $request,$project_id, $customizationRequest) {
        
        $dreq = CustomizationRequest::where('request_id',$customizationRequest)->first();
       
        $req = CustomizationRequest::
            where('series_no', $dreq->series_no)->where('project_id',$project_id)
            ->orderBy('created_at','asc')
            ->get();
       
       
            return view('requests.view_a_request',[
            'customizationRequests' => $req,
            'project_id' => $project_id,
            ]);
    }
    
    public function addRequest($project_id, $oldReq = null) {
        
        if($oldReq != null)
            $oldReq_obj = CustomizationRequest::where('request_id',$oldReq)->first();
        else $oldReq_obj = null;
        
        return view('requests.add_request',[
             'project_id' => $project_id,
             'oldReq' => $oldReq_obj,
             'groups' => Group::all(),
        ]);
    }
    
    public function modifyRequest(Request $request){
        
        $customizationRequest = CustomizationRequest::where('request_id', $request->edit_id)->first();
        
        $this->validate($request,[
                   'service_class' => 'required',
                   'size' => 'required',
                   'component' => 'required',
                   'existence' => 'required',
                   'dimension_table' => 'required',
                   'err_msg' => 'required',
                   'line_no' => 'required',
                   'pid' => 'required',
                   'area_code' => 'required',
                   'ident_code' => 'required',
            ]);
            $user_type = $request->user()->user_type;
            
            $customizationRequest->service_class = $request->service_class;
            $customizationRequest->size = $request->size;
            $customizationRequest->component = $request->component;
            $customizationRequest->description = $request->description;
            $customizationRequest->existence_in_service_class = $request->existence;
            $customizationRequest->dimension_table = $request->dimension_table;
            $customizationRequest->error_message = $request->err_msg;
            $customizationRequest->line_no = $request->line_no;
            $customizationRequest->pID_no = $request->pid;
            $customizationRequest->area_code = $request->area_code;
            $customizationRequest->ident_code = $request->ident_code;
            $customizationRequest->rating = $request->rating;
            
            $customizationRequest->project_id = $request->project_id;
            $customizationRequest->area_group = $request->area_group;
            
            $customizationRequest->series_no = $request->series_no;
            $user_type = $request->user()->user_type;
            $now = new \DateTime();
            if ($user_type == "requestor"){
                $customizationRequest->requestor_id = $request->user()->id;
                $customizationRequest->request_date = Carbon::now();
                $customizationRequest->status = "for approval";
            }
            else {
                $customizationRequest->requestor_id = $request->requestor_id;
                $customizationRequest->request_date = $request->request_date;
                $customizationRequest->requestor_remarks = $request->requestor_remarks;
            
                if ($user_type == "approver") {
                    $customizationRequest->approver_id = $request->user()->id;
                    $customizationRequest->approver_date = Carbon::now();
                    $customizationRequest->approval_state = $request->approval_state;
                    $customizationRequest->approver_remarks = $request->remarks;
                    if($request->approval_state == "yes") {
                        $customizationRequest->status = "for execution";    
                    }
                    else{
                        $customizationRequest->status = "disapproved";
                    }
                }
            
                else if ($user_type == "executor") {
                    $isRejected = false;
                    
                    $customizationRequest->approver_id = $request->approver_id;
                    $customizationRequest->approver_date = $request->approver_date;
                    $customizationRequest->approval_state = $request->approval_state;
                    $customizationRequest->approver_remarks = $request->approver_remarks;
                    
                    $customizationRequest->executor_id = $request->user()->id;
                    if($customizationRequest->execution_state == "yes" && $request->execution_state == "no"){
                        $isRejected = true;
                    }
                    $customizationRequest->execution_state = $request->execution_state;
                    $customizationRequest->execution_date = Carbon::now(); 
                    $customizationRequest->executor_remarks = $request->remarks;
                    
                    if($request->execution_state == "yes"){
                        $customizationRequest->status = "ongoing";
                    }
                    else if($isRejected){
                         $customizationRequest->status = "rejected";
                    }
                    else{
                        $customizationRequest->status = "disapproved";
                    }
                }
            }
           
            $customizationRequest->save();
            $this->addFile($request, $customizationRequest->request_id);
            $this->sendMail($request,$customizationRequest,'Request has been modified.');
            return $this->viewFromProject($request, $request->project_id);
    }
    
    public function editRequest($project_id, $oldReq) {
        
        $oldReq_obj = CustomizationRequest::where('request_id',$oldReq)->first();
        
        return view('requests.edit_a_request',[
             'project_id' => $project_id,
             'oldReq' => $oldReq_obj,
        ]);
    }
    
    public function sendMail(Request $request, $customizationRequest, $msg = null ) {
        if($request->user()->user_type == "requestor") {
                $users = Group::find($customizationRequest->area_group)->users()->where('user_type','approver')->get();
              
                foreach($users as $user) {
                    Mail::send('mail.toApprovers1', array('msg'=>$msg,'user_info'=>$user, 'request_info'=>$customizationRequest, 'requestor'=>$request->user()->fname.' '.$request->user()->lname), 
                    function($message) use ($user){
                     $message->to($user->email, $user->fname.' '. $user->lname)->subject('New Request');
                    });    
                    
                    if( count(Mail::failures()) > 0 ) {
                            $status = "failed";
                        } else {
                            $status = "success";
                        }
                    DB::table('email_logs')->insert([
                    'recepient_email'=> $user->email,
                    'status'=> $status,
                    ]);
                }
            }
            
            else if($request->user()->user_type == "approver") {
                $requestor = User::find($customizationRequest->requestor_id);
                $approver = User::find($customizationRequest->approver_id);
           
                Mail::send('mail.torequestor1', array('msg'=>$msg,'request_info'=>$customizationRequest,'requestor'=>$requestor,'approver'=>$approver), 
                function($message) use ($requestor){
                    $message->to($requestor->email, $requestor->fname.' '.$requestor->lname)->subject('Request Status Update');
                });
                
                if( count(Mail::failures()) > 0 ) {
                    $status = "failed";
                } else {
                    $status = "success";
                }
                DB::table('email_logs')->insert([
                'recepient_email'=> $requestor->email,
                'status'=> $status 
                ]);
                
                if($customizationRequest->status != "disapproved"){
                    $users = Group::find(1)->users()->where('user_type','executor')->get();
                    foreach($users as $user) {
                        Mail::send('mail.toexecutor', array('msg'=>$msg,'request_info'=>$customizationRequest,'requestor'=>$requestor,'approver'=>$approver), 
                        function($message) use ($user){
                            $message->to($user->email, $user->fname.' '.$user->lname)->subject('New Request');
                        });
                    
                        if( count(Mail::failures()) > 0 ) {
                                $status = "failed";
                            } else {
                                $status = "success";
                            }
                        DB::table('email_logs')->insert([
                        'recepient_email'=> $user->email,
                        'status'=> $status 
                        ]);
                    }
                }
            }
            
            else if($request->user()->user_type == "executor") {
                $requestor = User::find($customizationRequest->requestor_id);
                $approver = User::find($customizationRequest->approver_id);
                $executor = User::find($customizationRequest->executor_id);
                Mail::send('mail.torequestor2', array('msg'=>$msg,'request_info'=>$customizationRequest,'requestor'=>$requestor,'approver'=>$approver,'executor'=>$executor), 
                    function($message) use ($requestor){
                        $message->to($requestor->email, $requestor->fname.' '.$requestor->lname)->subject('Request Status Update');
                    });
                
                if( count(Mail::failures()) > 0 ) {
                    $status = "failed";
                } else {
                    $status = "success";
                }
                DB::table('email_logs')->insert([
                'recepient_email'=> $requestor->email,
                'status'=> $status
                ]);    
            }
            return;
    }
    
    public function addFile(Request $req, $request_id) {
		$files = $req->file('attachments');
        
        if (is_array($files) || is_object($files)) {
    		foreach($files as $file) {
    		    $extension = $file->getClientOriginalExtension();
        		Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));
        		$entry = new Fileentry();
        		$entry->mime = $file->getClientMimeType();
        		$entry->original_filename = $file->getClientOriginalName();
        		$entry->filename = $file->getFilename().'.'.$extension;
                $entry->user_id = $req->user()->id;
                $entry->request_id = $request_id;
        		$entry->save();
        		echo "saved";
            }
        }
		return;
	}
	
	public function completeRequest(Request $req, $request_id) {
	    $request = CustomizationRequest::find($request_id);
	    $request->status = "completed";
	    $request->execution_date= Carbon::now();
	    $request->save();
	    $this->sendMail($req,$customizationRequest,'Request has been modified.');
	    return redirect(route('viewOne', ['project_id' => $request->project_id,'customizationRequest'=>$request_id ]) );
	}
    
}
