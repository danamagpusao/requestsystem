<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Project;
use App\User;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function view () {
        if(Auth::user()->user_type == "admin"){
            $projects =  $this->viewAll();
        } 
        else {
            $projects = $this->viewAvailable();
        }
        
        return view('home', [
            'projects' => $projects,
        ]);
    }
    
    public function viewProjects() {
        $projects = $this->viewAll();
        return view('admin.view_projects', [
            'projects' => $projects,
        ]);
    }
    
    public function viewAll(){
        $projects = Project::all();
        return $projects;
    }
    
    public function viewAvailable(){
        $projects = Auth::user()->projects();
        return $projects;
    }
    
    public function remove(Request $request){
        //set archive attribute to 1;
    } 
    
    public function saveToSession(Request $request, $project_id) {
        $request->session()->put('project_id', $project_id);
    }

    public function addProject(Request $request) {
        DB::table('projects')->insert([
            'project_name'=> $request->pname,
            ]);
        return $this->viewProjects();
    }
    
    public function editProject ($project_id) {
        return view('admin.edit_project',[
            'project'=> Project::find($project_id),
        ]);  
    }
    
    public function saveProject (Request $request, $project_id) {
        $newProject = Project::find($project_id);
        $newProject->project_name = $request->pname;
        $newProject->save();
        return redirect(route('view_all_projects'));
    }
    
    public function removeMember($project_id, $member_id){
        DB::table('project_user')->where('project_id',$project_id)->where('user_id',$member_id)->delete();
        return redirect(route('edit_project',['project_id'=>$project_id]));
    }
    
}
