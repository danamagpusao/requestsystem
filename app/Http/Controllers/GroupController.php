<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Group;
use App\Http\Requests;

class GroupController extends Controller
{
    public function viewGroups () {
        return view('admin.view_groups',[
            'groups'=> Group::all(),
        ]);  
    }

    public function addGroup (Request $request) {
        DB::table('groups')->insert([
            'group_name'=> $request->gname,
            ]);
        return $this->viewGroups();
    }

    public function editGroup ($group_id) {
        return view('admin.edit_group',[
            'group'=> Group::find($group_id),
        ]);  
    }
    
    public function saveGroup (Request $request, $group_id) {
        $newGroup = Group::find($group_id);
        $newGroup->group_name = $request->gname;
        $newGroup->save();
        return redirect(route('view_groups'));
    }
    
    public function removeMember($group_id, $member_id){
        DB::table('user_group')->where('group_id',$group_id)->where('user_id',$member_id)->delete();
        return redirect(route('edit_group',['group_id'=>$group_id]));
    }
}
