<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;

class EmailController extends Controller
{
    public function getLogs () {
       $logs = DB::table('email_logs')->orderBy('created_at','desc')->get();
       return view('admin.email_logs', ['logs'=>$logs]);
    }
}
