<?php

namespace App\Http\Middleware;

use Closure;

class Inactivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!$request->user()->isActive) {
            return response('Your account has been deactivated', 401);
        } 
        return $next($request);
    }
}
