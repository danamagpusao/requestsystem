<?php

namespace App\Http\Middleware;

use Closure;
use App\User;

class Project
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $projects = User::projects();
        return $next($request);
    }
}
