<?php
use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('home');

// Authentication Routes...
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');

// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');


Route::group(['middleware'=>['auth', 'inactivity']], function () {
        
    
    Route::group(['prefix'=>'requests'], function(){
    
        Route::get('/', 'ProjectController@view')->name('view_projects');
        
        Route::get('/{project_id}/filter/{filter?}', [
            'uses' => 'CustomizationRequestController@viewFromProject',
            'as' => 'viewFromProject'
            ]);
        
        Route::get('/{project_id}/view/{customizationRequest}', [
            'uses' => 'CustomizationRequestController@viewOneRequest',
            'as' => 'viewOne'
            ]);
            
        Route::get('{project_id}/add/{oldReq?}', [
            'uses' => 'CustomizationRequestController@addRequest',
            'as' => 'add_request'
            ]);
            
        Route::get('{project_id}/edit/{oldReq}', [
            'uses' => 'CustomizationRequestController@editRequest',
            'as' => 'edit_request'
            ]);
            
        
    });
        
        Route::post('/store', [
                'uses' => 'CustomizationRequestController@storeRequest',
                'as' => 'store_request'
                ]);
        
        Route::post('/modify', [
                'uses' => 'CustomizationRequestController@modifyRequest',
                'as' => 'modify_request'
                ]);
                
        Route::post('/complete/{request_id}', [
                'uses' => 'CustomizationRequestController@completeRequest',
                'as' => 'complete_request'
                ]);
        
        
        Route::group(['prefix'=>'admin', 'middleware' => 'administrator'], function() {
            
            Route::get('/', function (){
                return view('admin.main');
            });
            
            Route::get('/email_logs', [
                'uses' => 'EmailController@getLogs',
                'as' => 'email_logs'
                ]);
        
            
            Route::get('/view_users', [
                'uses' => 'UsersController@view',
                'as' => 'view_users'
                ]);
            
            Route::get('/add_user', function(){
                return view('auth.register', [
                'projects' => App\Project::all(),
                'groups' => App\Group::all(),
                ]);
            })->name('add_user');
            
            Route::post('/save_user/', [
                'uses' => 'UsersController@saveUser',
                'as' => 'save_user'
            ]);
            
            Route::get('/edit_user/{user_id}', [
                'uses' => 'UsersController@editUser',
                'as' => 'edit_user',
                ]);
            
            Route::post('/update_user/{user_id}', [
                'uses' => 'UsersController@updateUser',
                'as' => 'update_user',
                ]);
                    
            Route::get('/activate/{user_id}', [
                'uses' => 'UsersController@activate',
                'as' => 'activate',
                ]);
                
            Route::get('/deactivate/{user_id}', [
                'uses' => 'UsersController@deactivate',
                'as' => 'deactivate',
                ]);
            
            Route::get('/view_projects', [
                'uses' => 'ProjectController@viewProjects',
                'as' => 'view_all_projects'
                ]);
            
            Route::post('/add_group', [
                'uses' => 'GroupController@addGroup',
                'as' => 'add_group'
                ]);

            Route::get('/view_groups', [
                'uses' => 'GroupController@viewGroups',
                'as' => 'view_groups'
                ]); 

            Route::get('/edit/group/{group_id}', [
                'uses' => 'GroupController@editGroup',
                'as' => 'edit_group'
                ]);
                
            Route::post('/save/group/{group_id}', [
                'uses' => 'GroupController@saveGroup',
                'as' => 'save_group'
                ]);
            
            Route::get('/remove/group/{group_id}/{user_id}', [
                'uses' => 'GroupController@removeMember',
                'as' => 'remove_member'
                ]);

            Route::post('/add_project', [
                'uses' => 'ProjectController@addProject',
                'as' => 'add_project'
                ]);
            Route::get('/edit/project/{project_id}', [
                'uses' => 'ProjectController@editProject',
                'as' => 'edit_project'
                ]);
                
            Route::post('/save/project/{project_id}', [
                'uses' => 'ProjectController@saveProject',
                'as' => 'save_project'
                ]);
            
            Route::get('/remove/project/{project_id}/{user_id}', [
                'uses' => 'ProjectController@removeMember',
                'as' => 'remove_pmember'
                ]);
        });
        
        Route::get('download/{filename}', [
        	'as' => 'download', 'uses' => 'FileEntryController@get']);
       
 
});