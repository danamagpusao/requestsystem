<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileAttachment extends Model
{
    public function customizationRequest(){
        return $this->belongsTo(CustomizationRequest::class);
    }
}
