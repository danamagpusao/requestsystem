<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     
        DB::table('users')->insert([
            'fname'=> 'Admin',
            'lname'=> 'Admin',
            'email'=> 'administrator@requestsystem.com',
            'password' => bcrypt('adminadmin'),
            'user_type' => 'admin'
            ]);
        
    }
}
