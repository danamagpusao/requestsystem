<?php

use Illuminate\Database\Seeder;

class ProjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('projects')->insert([
            'project_name'=> 'Remora',
            ]);
        DB::table('projects')->insert([
            'project_name'=> 'Octavia',
            ]);
        DB::table('projects')->insert([
            'project_name'=> 'Crylist',
            ]);
        DB::table('projects')->insert([
            'project_name'=> 'Amour',
            ]);
        DB::table('projects')->insert([
            'project_name'=> 'Idion',
            ]);
    }
}
