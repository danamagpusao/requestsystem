<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomizationRequestTable extends Migration
{
public function up()
    {
        Schema::create('customization_requests', function (Blueprint $table) {
            $table->increments('request_id');
            $table->string('series_no');
            $table->string('service_class');
            $table->string('size');
            $table->string('component');
            $table->text('description');
            $table->string('existence_in_service_class');
            $table->string('dimension_table');
            $table->string('error_message');
            $table->string('line_no');
            $table->string('pID_no');
            $table->string('area_code');
            $table->string('ident_code');
            $table->string('rating');
            $table->timestamps();
            //request status
            $table->integer('requestor_id');
            $table->timestamp('request_date')->default(null);
            $table->string('requestor_remarks');
            
            $table->integer('approver_id');
            $table->timestamp('approver_date')->default(null);
            $table->string('approval_state');
            $table->string('approver_remarks');
            
            //completion data
            $table->integer('executor_id');
            $table->string('execution_state');
            $table->timestamp('execution_date')->default(null);
            $table->text('executor_remarks');
            
            $table->string('completion_status');
            $table->timestamp('completion_date')->default(null);

            $table->string('area_group');
            
            $table->string('status');
            $table->integer('project_id');
 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customization_requests');
    }
}
